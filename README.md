# Observability_Test



## Getting started

This repo is a simple test script that runs some selenium tests and pushes to Sumologic

### Environment variable

| variable | description |
| --- | --- |
| SUMOLOGIC_ENDPOINT | Sumologic HTTP collector url |

### to run the test
you need to have ruby installed and bundler so you can install the gems needed
``` 
gem install bundler
bundle install
```

from there you can run the test using rspec:
```
rspec
```

by default this will run [spec/test_spec.rb](spec/test_spec.rb), which is the base version that does not push to sumologic

there are two template files:
| file | description |
| --- | --- |
| [template/raw_test_spec.rb](template/raw_test_spec.rb) | version of script without pushing to sumologic |
| [template/logged_test_spec.rb](template/logged_test_spec.rb) | version of script pushing to sumologic |

So you can switch back and forth between the two by copy/paste.

## Improvements
By default I do the rough quickest code to get it to work and iterate, at this point I've not iterated so fixes:
* [ ] remove writing to a file in [spec/sumo_logger.rb](spec/sumo_logger.rb)
* [ ] setup Gitlab CI to run daily

## Links
helpful reference links 

| link |
| --- |
| [Sumologic HTTP Collector setup documentation](https://help.sumologic.com/docs/send-data/hosted-collectors/http-source/) |



