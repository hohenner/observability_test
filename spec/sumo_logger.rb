require 'uri'
require 'net/http'
require 'json'

class SumoLogger
    attr_reader :stack
    def initialize
        @stack = {}
    end

    def start(action)
        raise "action #{action} already exists" if @stack.key? action

        @stack[action] = {start: Time.now}
    end

    def end(action)
        raise "action #{action} does not exist" unless @stack.key? action

        @stack[action][:end] = Time.now
    end

    def push_data
        @url = URI(ENV['SUMOLOGIC_ENDPOINT']) 
        file_name = 'test.log'
        body = ""
        file = File.open(file_name, 'w')
        @stack.each do |key,value| 
            data = {timestamp: value[:start].to_i, duration: (value[:end] - value[:start]).to_f, action: key}
            file.puts data.to_json
        end
        file.close

        https = Net::HTTP.new(@url.host, @url.port)
        https.use_ssl = true
        request = Net::HTTP::Post.new(@url)
        request.body = File.read(file_name)
        response = nil

        https.start do |http|
            response = http.request(request)
        end

        File.delete(file_name)
    end
    
end
