require 'pry'

url = 'https://www.hohenner.com/web/'
scroll_down = '.menu-scroll-down'
first_archive = '#archives-2 > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)'
second_archive = '#archives-2 > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(2) > a:nth-child(1)'
third_archive = '#archives-2 > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(3) > a:nth-child(1)'
fourth_archive = '#archives-2 > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(4) > a:nth-child(1)'

describe "the signin process", type: :feature do
  it "signs me in" do
    visit url
    page.find(scroll_down).click
    page.find(first_archive).click
    expect(page).to have_content 'MARCH 29, 2017'
    page.find(second_archive).click
    expect(page).to have_content 'FEBRUARY 1, 2017'
    page.find(third_archive).click
    expect(page).to have_content 'JANUARY 11, 2017'
    page.find(fourth_archive).click
    expect(page).to have_content 'DECEMBER 22, 2016'
  end
end