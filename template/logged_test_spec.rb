require 'pry'
require_relative 'sumo_logger'

url = 'https://www.hohenner.com/web/'
scroll_down = '.menu-scroll-down'
first_archive = '#archives-2 > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)'
second_archive = '#archives-2 > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(2) > a:nth-child(1)'
third_archive = '#archives-2 > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(3) > a:nth-child(1)'
fourth_archive = '#archives-2 > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(4) > a:nth-child(1)'

describe "the signin process", type: :feature do
    before do
        @logger = SumoLogger.new
    end

    it "signs me in" do
      @logger.start('total')
      @logger.start('launch')

      visit url

      @logger.end('launch')
      @logger.start('scroll')

      page.find(scroll_down).click

      @logger.end('scroll')
      @logger.start('first_archive')

      page.find(first_archive).click
      expect(page).to have_content 'MARCH 29, 2017'

      @logger.end('first_archive')
      @logger.start('second_archive')

      page.find(second_archive).click
      expect(page).to have_content 'FEBRUARY 1, 2017'

      @logger.end('second_archive')
      @logger.start('third_archive')

      page.find(third_archive).click
      expect(page).to have_content 'JANUARY 11, 2017'

      @logger.end('third_archive')
      @logger.start('fourth_archive')

      page.find(fourth_archive).click
      expect(page).to have_content 'DECEMBER 22, 2016'

      @logger.end('fourth_archive')
      @logger.end('total')
  
      @logger.push_data
    end
  end